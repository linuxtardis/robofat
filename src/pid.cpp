/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PID implementation.
 */

#include "pid.h"

float robofat::pid::process(float error, float dt) {
	state.integral += error * dt;
	float derivative;

	if (std::isnan(state.lastError)) { // check for warm-up
		derivative = 0;
	} else {
		derivative = filter((error - state.lastError) / dt);
	}
	state.lastError = error;
	return constants.kp * (error +
	                       constants.ki * state.integral +
	                       constants.kd * derivative);
}

robofat::pid::pid(float kp, float ki, float kd, float filter_actual, float filter_hist) {
	this->constants.kp = kp;
	this->constants.ki = ki;
	this->constants.kd = kd;

	float sum = filter_actual + filter_hist;
	this->filter_param.actualWeight = filter_actual / sum;
	this->filter_param.oldWeight = filter_hist / sum;

	this->state.reset();
}

robofat::pid::pid(pid_consts<float> &consts, pid_filter<float> &filter)
		: constants(consts), filter_param(filter) {
	this->state.reset();
}

float robofat::pid::filter(float val) {
	if (std::isnan(state.history))
		return state.history = val;
	return state.history = val * filter_param.actualWeight +
	                       state.history * filter_param.oldWeight;

}

robofat::pid_consts<float> &robofat::pid::get_constants() {
	return constants;
}

void robofat::pid::set_constants(pid_consts<float> &consts) {
	constants = consts;
}

robofat::pid_state<float> &robofat::pid::get_state() {
	return state;
}

void robofat::pid::set_state(pid_state<float> &state) {
	this->state = state;
}

robofat::pid_filter<float> &robofat::pid::get_filter() {
	return filter_param;
}

void robofat::pid::set_filter(pid_filter<float> &filter) {
	filter_param = filter;
}










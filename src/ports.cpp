/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Motor and sensor initialization code.
 */

#include "ports.h"

robofat::device_not_found::device_not_found(const std::string &message,
                                            const device_type &type,
                                            const address_type &port)
		: message(message),
		  type(type),
		  port(port) { }

device_type robofat::device_not_found::get_device_type() {
	return type;
}

address_type robofat::device_not_found::get_port() {
	return port;
}

ostream &robofat::device_not_found::operator<<(ostream &stream) {
	stream << "{exception:device_not_found;";
	stream << "message:" << message << ";";
	stream << "device_type:" << type << ";";
	stream << "port:" << port << "}";
	return stream;
}








# RoboFAT
RoboFAT is a small robotic library written in C++ for ev3dev.
It's intended primarily as a starting point for programs for robot
competitions such as Robosoutěž, therefore it contains common classes for these.
I've created this library mostly from the sources of my previous programs,
notably robo2016-zs.

RoboFAT stands for "Robotic Flying Apple Tree". Yes, it's a nonsense.

I don't expect this to gain much attention. I've created this just
for reusing the code between another my projects.

## Components
This library gives you these things:
 * **Basic configuration loader**
   * Simple text format - `key=value`, `# comments`
   * Internal storage is an unordered map with std::string as key and value
   * Helper functions for converting values to some other types
     * `get(key)` - returns `std::string`
     * `get_integer(key)`
     * `get_float(key)`
     * `get_double(key)`
     * `get_bool(key)`
     * `get_port(key)` - returns `ev3dev::address_type`
 * **PID controller implementation**
   * Has a modifiable state
   * Basic configurable derivative filtering via exponential weighted moving average
 * **Pilot for controlling robot based on speed difference between wheels** - "steer pilot"
   * ideal for driving a line follower
   * a `push` function - takes the difference between wheels and sets motors' speeds
 * **Pilot for controlling robot with basic commands** (move 2 metres forward, rotate 60° counterclockwise, ...) - "step pilot"
   * function turning the robot with gyroscopic sensor are available
     * `turn_only` - just rotate
     * `turn_p_step` - rotate and then correct until the error reported by the gyro is small enough
     * `turn_pid_linear` - rotate and regulate the speed all the time with PID with information from the gyro
     * `turn_arc_only` - move along an arc
     * `gyro_calibrate` - calibrate the gyro
   * simple move
     * `forward` - start motors forward
     * `backward` - start motors backward
     * `move` - move specified distance
     * `move_to_wall` - move until the touch sensor is pressed
     * `stop` - stop
   * a lot of settings
     * each wheel can be reversed
 * **Basic ncurses-based screen TUI** *(Optional)*
   * reading EV3 buttons
   * printing in ncurses window + centered print
   * init-system style logging
   * simple menu support
   * build without TUI/ncurses is supported
 * **Device loader (ports)**
   * Non-TUI version
     * Allocates the device on the heap and checks if it's connected.
     * If not, it's deleted and nullptr is returned.
   * TUI version
     * Calls non-TUI version
     * If the device isn't connected, it asks the user through TUI menu.
 * A bit more beginner-friendly LED abstraction over the ev3dev-lang API
 * Few utility functions for trimming strings and calculating delta time in loops.
 * a relatively small size (~1400 SLOC)

## Licence
I've released this software under the GNU GPL version 3.0 or later license. For details, see the LICENSE file.

## About me
I'm a hobbyist programmer and a student at the Gymnázium Kladno in the Czech Republic.
My favourite subjects are maths, science and ICT.
I happily participate in maths and physics competitions.
My most favourite competition is Robosoutěž. In this competition
you and two other people have to build and program a robot
to do a certain task in circa two months. Then the robots of all the teams
compete against each other on the competition itself. At the end, first four teams get
interesting prizes.
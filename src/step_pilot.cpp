/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * TuxovaTARDIS' Mechanics-style implementation of robot's physics - a stepped pilot.
 */

#include "step_pilot.h"
#include "util.h"
#include <unistd.h>
#include <ports.h>
#include <system_error>

using std::chrono::steady_clock;
using std::chrono::duration_cast;
using robofat::utility::inversion_sign;
using robofat::utility::calc_delta_us;
using robofat::utility::limit;
using robofat::utility::ftoi;
using robofat::utility::PI_F;

#ifdef HAS_CURSES

robofat::step_pilot::step_pilot(address_type &left_motor, address_type &right_motor, address_type *touch_addr,
                                address_type *gyro_addr, float track_width, float wheel_diameter, display *scr)
		: forward_rate(360.0f / (wheel_diameter * PI_F)),
		  turn_rate(track_width / wheel_diameter),
		  turn_linear_pid(1.0f, 0.0f, 0.0f, 1.0f, 0.0f),
		  track_width(track_width) {
	if (scr == nullptr) { // just initialization
		// motors
		left = get_device_heap<large_motor>(left_motor);
		right = get_device_heap<large_motor>(right_motor);
		if (left == nullptr)
			throw device_not_found("left motor not found", motor::motor_large, left_motor);
		if (right == nullptr)
			throw device_not_found("right motor not found", motor::motor_large, right_motor);
		// sensors
		if (touch_addr != nullptr) {
			auto &addr = *touch_addr;
			touch = get_device_heap<touch_sensor>(addr);
			if (touch == nullptr)
				throw device_not_found("touch sensor not found", sensor::ev3_touch, addr);
		} else touch = nullptr;

		if (gyro_addr != nullptr) {
			auto &addr = *gyro_addr;
			gyro = get_device_heap<gyro_sensor>(addr);
			if (touch == nullptr)
				throw device_not_found("gyro sensor not found", sensor::ev3_gyro, addr);
			gyro->set_mode(gyro_sensor::mode_gyro_ang);
		} else gyro = nullptr;

	} else { // initialize with user interaction
		display &disp = *scr;
		left = get_device_heap_prompt<large_motor>(left_motor, disp, true, "left motor");
		right = get_device_heap_prompt<large_motor>(right_motor, disp, true, "right motor");
		touch = touch_addr == nullptr ? nullptr :
		        get_device_heap_prompt<touch_sensor>(*touch_addr, disp, false, "touch sensor");
		gyro = gyro_addr == nullptr ? nullptr :
		       get_device_heap_prompt<gyro_sensor>(*gyro_addr, disp, false, "gyro sensor");
	}
}

#else
robofat::step_pilot::step_pilot(address_type &left_motor, address_type &right_motor, address_type *touch_addr,
								address_type *gyro_addr, float track_width, float wheel_diameter)
		: forward_rate(360.0f / (wheel_diameter * PI_F)),
		  turn_rate(track_width / wheel_diameter),
		  turn_linear_pid(1.0f, 0.0f, 0.0f, 1.0f, 0.0f),
		  track_width(track_width) {
		// motors
		left = get_device_heap<large_motor>(left_motor);
		right = get_device_heap<large_motor>(right_motor);
		if (left == nullptr)
			throw device_not_found("left motor not found", motor::motor_large, left_motor);
		if (right == nullptr)
			throw device_not_found("right motor not found", motor::motor_large, right_motor);
		// sensors
		if (touch_addr != nullptr) {
			auto &addr = *touch_addr;
			touch = get_device_heap<touch_sensor>(addr);
			if (touch == nullptr)
				throw device_not_found("touch sensor not found", sensor::ev3_touch, addr);
		} else touch = nullptr;

		if (gyro_addr != nullptr) {
			auto &addr = *gyro_addr;
			gyro = get_device_heap<gyro_sensor>(addr);
			if (touch == nullptr)
				throw device_not_found("gyro sensor not found", sensor::ev3_gyro, addr);
			gyro->set_mode(gyro_sensor::mode_gyro_ang);
		} else gyro = nullptr;
}
#endif

robofat::step_pilot::~step_pilot() {
	delete left;
	delete right;
	if (touch != nullptr)
		delete touch;
	if (gyro != nullptr)
		delete gyro;
}


void robofat::step_pilot::turn_only(int degrees_ccw, bool wait) {
	// calculate wheels
	int rot_ccw = ftoi(degrees_ccw * turn_rate);
	// prepare
	stop(true);
	push_speed(false, false, turn_mode);
	// update
	right->set_position_sp(rot_ccw);
	left->set_position_sp(-rot_ccw);
	// run
	right->run_to_rel_pos();
	left->run_to_rel_pos();
	// wait
	if (wait) {
		wait_move();
	}
}

void robofat::step_pilot::turn_p_step(int degrees_ccw, int max_error) {
	// no-gyro mode handling
	if (gyro == nullptr) {
		turn_only(degrees_ccw, true);
		return;
	}
	// perform first round
	stop(true);
	int gyro0 = gyro->angle();
	turn_only(degrees_ccw, true);
	// correct
	while (true) {
		int angle = gyro0 - gyro->angle();
		int error = degrees_ccw - angle;
		if (std::abs(error) < max_error)
			break;
		turn_only(ftoi(error * turn_step_kp), true);
	}
}

void robofat::step_pilot::turn_pid_linear(int degrees_ccw, int max_error) {
	// no-gyro mode handling
	if (gyro == nullptr) {
		turn_only(degrees_ccw, true);
		return;
	}
	// prepare
	stop(true);
	if (degrees_ccw == 0)
		return;
	turn_linear_pid.get_state().reset();
	int gyro0 = gyro->angle();
	instant last = steady_clock::now();
	// loop until the error is small enough
	while (true) { // speed regulation
		int angle = gyro0 - gyro->angle();
		int error = degrees_ccw - angle;
		if (std::abs(error) < max_error)
			break;
		float dt = calc_delta_us(last) / 1000.0f;

		int speed = ftoi(turn_linear_pid.process(error, dt));
		speed = limit(speed, -this->speed_turn, this->speed_turn);
		push_speed(speed, true, false);
		right->run_forever();
		left->run_forever();
	}
	stop(true);
}

void robofat::step_pilot::turn_arc_only(int spec, int radius, arc_mode mode, bool wait) {
	// check for invalid input
	if (radius == 0) {
		if (mode == angle)
			throw std::runtime_error("can't use angle for straight move!");
		move(spec, true);
		return;
	}
	// assign correct motors to sides
	large_motor *in, *out;
	if (radius > 0) { // if counter-clockwise
		in = left;
		out = right;
	} else {
		in = right;
		out = left;
	}
	// calculate speeds and distances inside and outside
	radius = std::abs(radius);
	float dist = mode == angle ? spec / 360.0f * 2 * PI_F * radius : spec;
	stop(true);
	float ratio = (radius - track_width / 2) / radius; // ratio_in
	// update inside
	in->set_speed_sp(/*    */ftoi(ratio * speed_fwd));
	in->set_position_sp(/* */ftoi(ratio * dist));
	ratio = (radius + track_width / 2) / radius; // ratio_out
	// update outside
	out->set_speed_sp(/*   */ftoi(ratio * speed_fwd));
	out->set_position_sp(/**/ftoi(ratio * dist));
	// run
	out->run_to_rel_pos();
	in->run_to_rel_pos();
	// wait
	if (wait) {
		wait_move();
	}
}

void robofat::step_pilot::move(int distance, bool wait) {
	int deg = ftoi(distance * forward_rate);
	stop(true);
	push_speed(false, false);
	right->set_position_sp(deg);
	left->set_position_sp(deg);
	right->run_to_rel_pos();
	left->run_to_rel_pos();
	if (wait) {
		wait_move();
	}
}

void robofat::step_pilot::forward() {
	stop(true);
	push_speed(false, false);
	right->run_forever();
	left->run_forever();
}

void robofat::step_pilot::backward() {
	stop(true);
	push_speed(true, true);
	right->run_forever();
	left->run_forever();
}

void robofat::step_pilot::move_to_wall() {
	if (touch == nullptr)
		return;
	forward();
	while (!touch->is_pressed()) {
		usleep(POLL_USEC);
	}
	stop(true);
}

void robofat::step_pilot::wait_move() const {
	ev3dev::mode_set left_state;
	ev3dev::mode_set right_state;
	// loop until motors stop
	while (true) {
		left_state = this->left->state();
		right_state = this->right->state();
		if ((left_state.find("running") != left_state.end() ||
		     left_state.find("stalled") == left_state.end()) &&
		    (right_state.find("running") != right_state.end() &&
		     right_state.find("stalled") == right_state.end())) {
			break;
		}
		usleep(robofat::step_pilot::POLL_USEC);
	}
}

void robofat::step_pilot::stop(bool wait) {
	right->stop();
	left->stop();
	if (wait) {
		ev3dev::mode_set left_state;
		ev3dev::mode_set right_state;
		// loop until motors stop
		while (true) {
			left_state = left->state();
			right_state = right->state();
			if (left_state.find("running") != left_state.end() &&
			    right_state.find("running") != right_state.end()) {
				break;
			}
			usleep(POLL_USEC);
		}
	}
}


void robofat::step_pilot::gyro_calibrate() {
	if (gyro == nullptr)
		return;
	gyro->set_mode(gyro_sensor::mode_gyro_rate);
	usleep(1000000);
	gyro->set_mode(gyro_sensor::mode_gyro_ang);
}

void robofat::step_pilot::push_speed(bool left_reverse, bool right_reverse, const speed_mode mode) {
	int val;
	if (mode == turn_mode)
		val = std::abs(speed_turn);
	else
		val = std::abs(speed_fwd);
	right->set_speed_sp(val * inversion_sign(right_reverse));
	left->set_speed_sp(val * inversion_sign(left_reverse));
}

void robofat::step_pilot::push_speed(int value, bool left_reverse, bool right_reverse) {
	right->set_speed_sp(value * inversion_sign(right_reverse));
	left->set_speed_sp(value * inversion_sign(left_reverse));
}

int robofat::step_pilot::get_ramp_up() {
	return left->ramp_up_sp();
}

int robofat::step_pilot::get_ramp_down() {
	return left->ramp_down_sp();
}

void robofat::step_pilot::set_ramp_up(int time) {
	right->set_ramp_up_sp(time);
	left->set_ramp_up_sp(time);
}

void robofat::step_pilot::set_ramp_down(int time) {
	right->set_ramp_down_sp(time);
	left->set_ramp_down_sp(time);
}


int robofat::step_pilot::get_speed_move() {
	return ftoi(speed_fwd / forward_rate);
}

void robofat::step_pilot::set_speed_move(int unitsPerSec) {
	speed_fwd = ftoi(unitsPerSec * forward_rate);
}

int robofat::step_pilot::get_speed_turn() {
	return ftoi(speed_turn / turn_rate);
}

void robofat::step_pilot::set_speed_turn(int degPerSec) {
	speed_turn = ftoi(degPerSec * turn_rate);
}


void robofat::step_pilot::set_position_pid(pid_consts<int> &constants) {
	right->set_position_p(constants.kp);
	right->set_position_i(constants.ki);
	right->set_position_d(constants.kd);
	left->set_position_p(constants.kp);
	left->set_position_i(constants.ki);
	left->set_position_d(constants.kd);
}

void robofat::step_pilot::set_speed_pid(pid_consts<int> &constants) {
	right->set_speed_p(constants.kp);
	right->set_speed_i(constants.ki);
	right->set_speed_d(constants.kd);
	left->set_speed_p(constants.kp);
	left->set_speed_i(constants.ki);
	left->set_speed_d(constants.kd);
}

robofat::pid_consts<int> robofat::step_pilot::get_position_pid() {
	pid_consts<int> retval;
	retval.kp = left->position_p();
	retval.ki = left->position_i();
	retval.kd = left->position_d();
	return retval;
}

robofat::pid_consts<int> robofat::step_pilot::get_speed_pid() {
	pid_consts<int> retval;
	retval.kp = left->speed_p();
	retval.ki = left->speed_i();
	retval.kd = left->speed_d();
	return retval;
}

bool robofat::step_pilot::get_left_invert() {
	return left->polarity() == motor::encoder_polarity_inversed;
}

bool robofat::step_pilot::get_right_invert() {
	return right->polarity() == motor::encoder_polarity_inversed;
}

void robofat::step_pilot::set_left_invert(bool invert) {
	std::string toSet;
	if (invert)
		toSet = motor::encoder_polarity_inversed;
	else
		toSet = motor::encoder_polarity_normal;
	left->set_polarity(toSet);
}

void robofat::step_pilot::set_right_invert(bool invert) {
	std::string toSet;
	if (invert)
		toSet = motor::encoder_polarity_inversed;
	else
		toSet = motor::encoder_polarity_normal;
	right->set_polarity(toSet);
}


float robofat::step_pilot::get_turn_step_kp() {
	return turn_step_kp;
}

void robofat::step_pilot::set_turn_step_kp(float value) {
	turn_step_kp = value;
}

robofat::pid robofat::step_pilot::get_turn_linear_pid() {
	return turn_linear_pid;
}

/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Differential motor controller implementation.
 */

#include <unistd.h>
#include <ports.h>
#include "steer_pilot.h"
#include "util.h"

using robofat::utility::signum;
using robofat::utility::limit;
#ifdef HAS_CURSES

robofat::steer_pilot::steer_pilot(const address_type &left,
                                  const address_type &right,
                                  display *scr) {
	if (scr == nullptr) { // just initialization
		leftMotor = get_device_heap<large_motor>(left);
		rightMotor = get_device_heap<large_motor>(right);
		if (leftMotor == nullptr)
			throw device_not_found("left motor not found", motor::motor_large, left);
		if (rightMotor == nullptr)
			throw device_not_found("right motor not found", motor::motor_large, right);
	} else { // initialize with user interacton
		display &disp = *scr;
		leftMotor = get_device_heap_prompt<large_motor>(left, disp, true, "left motor");
		rightMotor = get_device_heap_prompt<large_motor>(right, disp, true, "right motor");
	}
    leftMotor ->set_stop_action(ev3dev::large_motor::stop_action_brake);
    rightMotor->set_stop_action(ev3dev::large_motor::stop_action_brake);
}

#else

robofat::steer_pilot::steer_pilot(const address_type &left,
								  const address_type &right) {
	leftMotor = get_device_heap<large_motor>(left);
	rightMotor = get_device_heap<large_motor>(right);
	if (leftMotor == nullptr)
		throw device_not_found("left motor not found", motor::motor_large, left);
	if (rightMotor == nullptr)
		throw device_not_found("right motor not found", motor::motor_large, right);
}

#endif

void robofat::steer_pilot::push(float ccw) {
	// add/subtract difference to/from base speed
	int leftSpeed = (int) roundf(defaultSpeed - ccw);
	int rightSpeed = (int) roundf(defaultSpeed + ccw);
	// invert if inverted
	if (leftInvert)
		leftSpeed *= -1;
	if (rightInvert)
		rightSpeed *= -1;
	// check overflow
	if (clipFlow) {
		int over, sign;
		if (abs(leftSpeed) > maxSpeed) {
			sign = signum(leftSpeed);
			over = leftSpeed - maxSpeed * sign;
			leftSpeed = maxSpeed * sign;
			rightSpeed -= over;
		} else if (abs(rightSpeed) > maxSpeed) {
			sign = signum(rightSpeed);
			over = rightSpeed - maxSpeed * sign;
			rightSpeed = maxSpeed * sign;
			leftSpeed -= over;
		}
	}
	// final clipping
	leftSpeed = limit(leftSpeed, -maxSpeed, maxSpeed);
	rightSpeed = limit(rightSpeed, -maxSpeed, maxSpeed);
	// push
	push_power(leftMotor, leftSpeed);
	push_power(rightMotor, rightSpeed);
}

void robofat::steer_pilot::turn(int ccw, long wait, bool waitComplete, bool stop) {
    int lSpeed = ( leftInvert ? -1 : 1) * defaultSpeed * -signum(ccw);
    int rSpeed = (rightInvert ? -1 : 1) * defaultSpeed * +signum(ccw);
    lSpeed = limit(lSpeed, -maxSpeed, +maxSpeed);
    rSpeed = limit(rSpeed, -maxSpeed, +maxSpeed);

    int  leftAngle =  leftInvert ? +ccw : -ccw;
    int rightAngle = rightInvert ? -ccw : +ccw;

    if (stop) {
        leftMotor->set_speed_sp(lSpeed);
        rightMotor->set_speed_sp(rSpeed);
        leftMotor->set_position_sp(leftAngle);
        rightMotor->set_position_sp(rightAngle);
        leftMotor->run_to_rel_pos();
        rightMotor->run_to_rel_pos();
        if (waitComplete) {
            // loop until motors stop
            waitStop(wait);
        }
    } else {
        push_power( leftMotor, lSpeed);
        push_power(rightMotor, rSpeed);

        int lDest =  leftMotor->position() +  leftAngle;
        int rDest = rightMotor->position() + rightAngle;

        if (waitComplete) {
            waitMove(lSpeed > 0, rSpeed > 0, lDest, rDest, wait);
        }
    }
}

void robofat::steer_pilot::travel(int dist, long wait, bool waitComplete, bool stop) {
    int lSpeed = ( leftInvert ? -1 : 1) * defaultSpeed;
    int rSpeed = (rightInvert ? -1 : 1) * defaultSpeed;
    lSpeed = limit(lSpeed, -maxSpeed, +maxSpeed);
    rSpeed = limit(rSpeed, -maxSpeed, +maxSpeed);
	if (stop) {
		 leftMotor->set_speed_sp(lSpeed);
		rightMotor->set_speed_sp(rSpeed);
		 leftMotor->set_position_sp(dist);
		rightMotor->set_position_sp(dist);
		 leftMotor->run_to_rel_pos();
		rightMotor->run_to_rel_pos();
		if (waitComplete) {
			// loop until motors stop
            waitStop(wait);
		}
	} else {
		if (dist < 0) {
			lSpeed = -lSpeed;
			rSpeed = -rSpeed;
		}
		push_power( leftMotor, lSpeed);
		push_power(rightMotor, rSpeed);

		int leftAngle  = leftInvert  ? -dist : +dist;
		int rightAngle = rightInvert ? -dist : +dist;
		int lDest =  leftMotor->position() +  leftAngle;
		int rDest = rightMotor->position() + rightAngle;
		if (waitComplete)
			waitMove(lSpeed > 0, rSpeed > 0, lDest, rDest, wait);
	}
}

void robofat::steer_pilot::arc(int dist, float ccw, long wait) {
    int leftAngle  = ( leftInvert ? -1 : + 1) * dist * (defaultSpeed - ccw) / defaultSpeed;
    int rightAngle = (rightInvert ? -1 : + 1) * dist * (defaultSpeed + ccw) / defaultSpeed;
    int lDest =  leftMotor->position() +  leftAngle;
    int rDest = rightMotor->position() + rightAngle;

    push(ccw);

    waitMove( leftMotor->speed_sp() > 0,
             rightMotor->speed_sp() > 0,
             lDest, rDest, wait);
}

void robofat::steer_pilot::waitStop(long wait) {
    ev3dev::mode_set left_state;
    ev3dev::mode_set right_state;
    while (true) {
        left_state =  leftMotor->state();
        right_state = rightMotor->state();
        if ( left_state.find("running") ==  left_state.end() &&
             right_state.find("running") == right_state.end()) {
            break;
        }
        usleep(wait);
    }
}

void robofat::steer_pilot::waitMove(bool lFwd, bool rFwd, int lDest, int rDest, long wait) {
    while (true) {
        int lDone =  leftMotor->position();
        int rDone = rightMotor->position();
        bool lOK = lFwd ? lDone > lDest : lDone < lDest;
        bool rOK = rFwd ? rDone > rDest : rDone < rDest;
        if (lOK && rOK)
            break;
        usleep(wait);
    }
}

void robofat::steer_pilot::push_power(ev3dev::large_motor *motor, int speed) {
	motor->set_speed_sp(speed);
	motor->run_forever();
}

void robofat::steer_pilot::stop() {
	leftMotor->stop();
	rightMotor->stop();
}

robofat::steer_pilot::~steer_pilot() {
	delete leftMotor;
	delete rightMotor;
}

bool robofat::steer_pilot::get_left_invert() {
	return leftInvert;
}

bool robofat::steer_pilot::get_right_invert() {
	return rightInvert;
}

void robofat::steer_pilot::set_left_invert(bool invert) {
	leftInvert = invert;
}

void robofat::steer_pilot::set_right_invert(bool invert) {
	rightInvert = invert;
}

int robofat::steer_pilot::get_base_speed() {
	return defaultSpeed;
}

int robofat::steer_pilot::get_max_speed() {
	return maxSpeed;
}

bool robofat::steer_pilot::get_clip_overflow() {
	return clipFlow;
}

void robofat::steer_pilot::set_base_speed(int speed) {
	defaultSpeed = speed;
}

void robofat::steer_pilot::set_max_speed(int speed) {
	int lMax =  leftMotor->max_speed();
	int rMax = rightMotor->max_speed();
	int mMax = std::min(lMax, rMax);
	maxSpeed = std::min(speed, mMax);
}

void robofat::steer_pilot::enable_clip_overflow(bool enabled) {
	clipFlow = enabled;
}

int robofat::steer_pilot::get_ramp_up() {
	return leftMotor->ramp_up_sp();
}

int robofat::steer_pilot::get_ramp_down() {
	return leftMotor->ramp_down_sp();
}

void robofat::steer_pilot::set_ramp_up(int time) {
	rightMotor->set_ramp_up_sp(time);
	leftMotor->set_ramp_up_sp(time);
}

void robofat::steer_pilot::set_ramp_down(int time) {
	rightMotor->set_ramp_down_sp(time);
	leftMotor->set_ramp_down_sp(time);
}

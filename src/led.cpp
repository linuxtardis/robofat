/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * LED abstraction of ev3dev led subsystem.
 */

#include "led.h"
#include <unordered_map>

typedef std::vector<float> led_state;

led_state get_color_vector(robofat::led_color color);

void robofat::led_off() {
	ev3dev::led::all_off();
}

void robofat::led_set(led_color left, led_color right) {
	led_state left_set = get_color_vector(left);
	led_state right_set = get_color_vector(right);
	ev3dev::led::set_color(ev3dev::led::left, left_set);
	ev3dev::led::set_color(ev3dev::led::right, right_set);
}

led_state get_color_vector(robofat::led_color color) {
	switch (color) {
		case robofat::led_color::RED:
			return ev3dev::led::red;
		case robofat::led_color::GREEN:
			return ev3dev::led::green;
		case robofat::led_color::AMBER:
			return ev3dev::led::amber;
		case robofat::led_color::ORANGE:
			return ev3dev::led::orange;
		case robofat::led_color::YELLOW:
			return ev3dev::led::yellow;
		default:
		case robofat::led_color::BLACK:
			return ev3dev::led::black;
	}
}

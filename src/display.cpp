/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Screen I/O implementation.
 */


#include <cstring>
#include "display.h"

#ifdef HAS_CURSES

robofat::display::display() {
	initscr(); // init ncurses mode
	cbreak(); // raw mode, except for Ctrl+C etc.
	noecho(); // disable echo
	curs_set(0); // hide cursor
	scrollok(stdscr, true); // enable scrolling
	keypad(stdscr, true); // enable keypad

	clear_display(); // clear screen

	// get width and height
	getmaxyx(stdscr, height, width);

}


robofat::display::~display() {
	endwin(); // end ncurses mode
}

robofat::display::keys robofat::display::getkey() {
	return getkey(stdscr);
}

robofat::display::keys robofat::display::getkey(WINDOW *wnd) {
	int ch = wgetch(wnd);
	if (ch == ERR || (
		ch != KEY_UP &&
	    ch != KEY_DOWN &&
	    ch != KEY_RIGHT &&
	    ch != KEY_LEFT &&
	    ch != KEY_ENTER && ch != static_cast<int>('\n') &&
	    ch != KEY_BACKSPACE)) {
		return keys::key_unknown;
	} else {
		if (ch == static_cast<int>('\n'))
			return key_enter;
		return (keys) ch;
	}
}

void robofat::display::print(const std::string &str, attrib attributes) {
	int y = getcury(stdscr);
	newline(y);
	attron(attributes);
	mvprintw(y, 0, "%s", str.c_str());
	attroff(attributes);
	refresh();
}

void robofat::display::init_end(bool success) {
	if (msg_y < 0)
		return;
	// copy status string
	char str[3];
	if (success)
		strncpy(str, "OK", 3);
	else
		strncpy(str, "EE", 3);
	// overwrite old waiting status
	mvprintw(msg_y, width - 2, str);
	// clean-up after init print
	move(getcury(stdscr), 0);
	refresh();
	msg_y = 0;
}


void robofat::display::init_msg(const std::string &message) {
	int strw = width - 5;
	int y = getcury(stdscr);
	newline(y);
	mvprintw(y, 0, "- %-*.*s", strw, strw, message.c_str());
	refresh();
}


void robofat::display::init_begin(const std::string &part) {
	msg_y = getcury(stdscr);
	int strw = width - 3;
	if (msg_y == height - 1) {
		scroll(stdscr);
	} else {
		msg_y++;
	}
	mvprintw(msg_y, 0, "%-*.*s|..", strw, strw, part.c_str());
	refresh();
}

void robofat::display::init_log(const std::string &msg) {
	int strw = width - 3;
	int y = getcury(stdscr);
	newline(y);
	mvprintw(y, 0, "%-*.*s|II", strw, strw, msg.c_str());
	refresh();
}

void robofat::display::print_centered(const std::string &str, attrib attributes) {
	int begin = (width - str.length()) / 2;
	if (begin < 0)
		begin = 0;
	int y = getcury(stdscr);
	newline(y);
	attron(attributes);
	mvprintw(y, begin, "%s\n", str.c_str());
	attroff(attributes);
	refresh();
}

int robofat::display::show_menu(const std::string &prompt,
                                const std::vector<std::string> &options,
                                int select,
                                bool can_exit) {
	// zero options is not valid
	if (options.size() == 0)
		return -1;
	// sanitize default selection
	if (select >= options.size())
		select = options.size() - 1;
	if (select < 0)
		select = 0;
	// get longest string
	int longest = get_longest(prompt, options);
	// calculate left side
	int first_x = (width - longest) / 2;
	if (first_x < 2)
		first_x = 2;
	// calculate top side
	int first_list_y = (height - options.size()) / 2;
	if (first_list_y < 2)
		first_list_y = 2;
	// create new window
	WINDOW *wnd = newwin(height, width, 0, 0);
	// enable keypad
	keypad(wnd, true);
	// draw border
	box(wnd, 0, 0);
	touchwin(wnd);
	// event loop
	while (true) {
		// draw
		menu_draw(wnd, prompt, options, select, first_list_y, first_x);
		wrefresh(wnd);
		// get input
		keys key;
		do {
			key = getkey(wnd);
		} while (key == key_unknown);
		// process input
		switch (key) {
			case key_up:
				select--;
				if (select < 0)
					select = options.size() - 1;
				break;
			case key_down:
				select++;
				if (select >= options.size())
					select = 0;
				break;
			case key_enter:
				goto endloop;
			case key_escape:
				if (can_exit) {
					select = -1;
					goto endloop;
				}
				break;
			case key_left:
			case key_unknown:
			case key_right:
				break;
		}
	}
	// cleanup
	endloop:
	wclear(wnd);
	wrefresh(wnd);
	delwin(wnd);
	touchwin(stdscr);
	refresh();
	return select;
}

void robofat::display::menu_draw(WINDOW *wnd,
                                 const std::string &prompt,
                                 const std::vector<std::string> &options,
                                 const int select,
                                 const int first_list_y,
                                 const int first_x) {
	// print prompt
	wattron(wnd, A_BOLD);
	mvwprintw(wnd, first_list_y - 1, first_x, "%s", prompt.c_str());
	wattroff(wnd, A_BOLD);
	// print options
	for (int i = 0; i < options.size(); i++) {
		if (select == i)
			wattron(wnd, A_REVERSE);
		mvwprintw(wnd, first_list_y + i, first_x + 1, "%s", options[i].c_str());
		if (select == i)
			wattroff(wnd, A_REVERSE);
	}
}

int robofat::display::get_longest(const std::string &prompt,
                                  const std::vector<std::string> &options) {
	int max = prompt.size();
	for (auto &str : options) {
		int length = str.length();
		if (max < length)
			max = length;
	}
	return max;
}

void robofat::display::clear_display() {
	clear();
	move(0, 0);
}

void robofat::display::newline(int &y) {
	if (y == height - 1) {
		scroll(stdscr);
		msg_y--;
	} else {
		y++;
	}
}

void robofat::display::settimeout(int timeout) {
	wtimeout(stdscr, timeout);
}

#else
#include <iostream>

robofat::display::display() {}
robofat::display::~display() {}

void robofat::display::print(const std::string &str) {
	std::cout << str << std::endl;
}

void robofat::display::init_end(bool success) {
	std::string status = success ? "OK" : "EE";
	std::cout << status << "|" << last_part << std::endl;
}

void robofat::display::init_msg(const std::string &message) {
	std::cout << "  |->" << message << std::endl;
}

void robofat::display::init_begin(const std::string &part) {
	std::cout << "..|" << part << std::endl;
	last_part = part;
}

void robofat::display::init_log(const std::string &msg) {
	std::cout << "II|" << msg << std::endl;
}
#endif
